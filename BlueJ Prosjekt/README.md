# Legoprosjekt i INGG1001 Ingeniørfaglig innføringsemne

Dette er repositoriet for Legoprosjektet i INGG1001 Ingeniørfaglig innføringsemne med bruk av BlueJ. Utlevert microSDHC-kort skal være et ferdig komplett oppsett med [ev3dev](https://www.ev3dev.org/) og [Java](http://ev3dev-lang-java.github.io/).

## Kom i gang

1. Sett microSDHC inn i EV3, start den, og se at den booter ev3dev.

2. Sett opp internet via USB som beskrevet her: https://www.ev3dev.org/docs/tutorials/connecting-to-the-internet-via-usb/, og finn IP-addressen til EV3 øverst på skjermen dens.

3. Sjekk at kommunikasjon med enheten via SSH fungerer: https://www.ev3dev.org/docs/tutorials/connecting-to-ev3dev-with-ssh/.

4. Last ned dette prosjektet som zip-fil og pakk det ut på egnet sted på egen maskin. (Eller lag en git-klon av det, hvis du skulle være kjent med git fra tidligere.)

5. Åpne prosjektet i BlueJ.

6. Legg til alle bibliotekene i `lib`-katalogen til CLASSPATH-en ved å velge `Tools/Preferences.../Libraries`og `Add File`.

7. Restart BlueJ (!) og kompiler på vanlig måte ved å trykke på «compile»-knappen på venstresiden.

8. Lag en jar-fil ved å velge `Project/Create JAR File`. Velg LegoRobot som `Main Class` og kryss av for å inkludere alle bibliotekene. Behøver ikke inkludere kildekode og prosjektfiler.

9. Overfør jar-fil til EV3 ved å velge `Tools/ev3dev`. Sett inn riktig IP-addresse for EV3 (skal stå øverst på skjermen dens), brukernavn og passord («robot» og «maker» hvis dere har fulgt oppskriftene slavisk), velg jar-filen du akkurat lagde, trykk `Deploy` og kryss fingrene.

10. EV3 kan nå kobles fra maskinen, og programmet kan kjøres ved å velge navnet du ga jar-filen fra filmenyen.

Dersom alt dette gikk bra (?!) er du nå klar til å begynne å bygge og programmere roboter.

## Veien videre

Dokumentasjonen for ev3dev-lang-java og lejos-bibliotekene: http://ev3dev-lang-java.github.io/docs/api/latest/index.html

Koden for BlueJ-plugin-en: https://git.gvk.idi.ntnu.no/tools/bluej-ev3dev-plugin

Et mer profesjonelt oppsett for ev3dev og Java med Gradle som byggesystem finnes her: https://git.gvk.idi.ntnu.no/course/ingg1001/ingg1001-legoprosjekt-gradle