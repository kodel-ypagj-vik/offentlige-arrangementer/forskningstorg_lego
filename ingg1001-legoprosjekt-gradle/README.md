# Legoprosjekt i INGG1001 Ingeniørfaglig innføringsemne

Dette repositoriet er et mer profesjonelt oppsett for Legoprosjektet, og er basert på denne malen: https://github.com/ev3dev-lang-java/template-project-gradle. Utlevert microSDHC-kort skal være et ferdig komplett oppsett med [ev3dev](https://www.ev3dev.org/) og [Java](http://ev3dev-lang-java.github.io/). Det enklere BlueJ-oppsettet finnes her: https://git.gvk.idi.ntnu.no/course/ingg1001/ingg1001-legoprosjekt-bluej.

## Kom i gang

1. Sett microSDHC inn i EV3, start den, og se at den booter ev3dev.

2. Sett opp internet via USB som beskrevet her: https://www.ev3dev.org/docs/tutorials/connecting-to-the-internet-via-usb/.

3. Sjekk at kommunikasjon med enheten via SSH fungerer: https://www.ev3dev.org/docs/tutorials/connecting-to-ev3dev-with-ssh/.

4. Installer _Java versjon 8_ på egen maskin:

    * Ubuntu: `sudo apt install openjdk-8-jdk`
    * Øvrige platformer (Windows, MacOS, ...): https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html

5. Installer utviklingsmiljøet IntelliJ på egen maskin: https://www.jetbrains.com/idea/. Som NTNU-student vil du ha tilgang til fullversjonen hvis du registrerer deg med NTNU-e-postadresse. Ellers holder «CE – Community Edition» i massevis for dette prosjektet.

6. Last ned dette prosjektet som zip-fil og pakk det ut på egnet sted på egen maskin. (Eller lag en git-klon av det, hvis du skulle være kjent med git fra tidligere.)

7. Start IntelliJ, velg «Import project» og importer prosjektet _ved å velge filen_ `build.gradle`.

8. Åpne filen `config.gradle` og endre om nødvendig IP-adressen i sshHost til den som vises på skjermen til EV3-en.

9. Velg `Gradle` i høyremargen, og kjør følgende Tasks:

    - `Tasks/build/build` for å kompilere programmet `src/main/java/ingg1001/LegoRobot`    
    - `Tasks/elj-deployment/testConnection` for å sjekke forbindelsen til EV3
    - `Tasks/elj-deployment/deploy` for å overføre det kompilerte programmet til EV3
    - `Tasks/elj-deployment/run` (med en stor motor koblet i port A) for å kjøre programmet

10. EV3 kan nå kobles fra maskinen, og programmet kan kjøres ved å velge `lego_robot-0.0.1.sh` fra filmenyen.

Dersom alt dette gikk bra (?!) er du nå klar til å begynne å bygge og programmere roboter. Dokumentasjonen for ev3dev-lang-java og lejos-bibliotekene: http://ev3dev-lang-java.github.io/docs/api/latest/index.html
