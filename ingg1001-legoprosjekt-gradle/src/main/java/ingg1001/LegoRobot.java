package ingg1001;
import ev3dev.actuators.lego.motors.*;
import ev3dev.sensors.ev3.EV3ColorSensor;
import ev3dev.sensors.ev3.EV3IRSensor;
import ev3dev.sensors.ev3.EV3UltrasonicSensor;
import lejos.hardware.port.MotorPort;
import lejos.robotics.*;
import lejos.hardware.port.SensorPort;
import java.lang.Thread;

public class LegoRobot {
    private static final EV3LargeRegulatedMotor motorC = new EV3LargeRegulatedMotor(MotorPort.C);
    private static final EV3LargeRegulatedMotor motorB = new EV3LargeRegulatedMotor(MotorPort.B);
    private static final EV3MediumRegulatedMotor brikkeMotor = new EV3MediumRegulatedMotor(MotorPort.A);
    private static final EV3ColorSensor colorSensor = new EV3ColorSensor(SensorPort.S3);
    private static final EV3UltrasonicSensor distanceSensor = new EV3UltrasonicSensor(SensorPort.S4);
    private static final SampleProvider sp = distanceSensor.getDistanceMode();

    enum Color {
        NONE,
        BLACK,
        BLUE,
        GREEN,
        YELLOW,
        RED,
        WHITE,
        BROWN
    }

    public static void main(String[] args) throws InterruptedException {
        //SETUP
        motorC.setSpeed(200);
        motorB.setSpeed(200);
        brikkeMotor.setSpeed(1000);
        brikkeMotor.rotateTo(40, false);
        colorSensor.setCurrentMode("ColorID");
        Color color;
        int currentDistance;
        do {
            currentDistance = getDistance();
            System.out.println("Driving forward to beacon-point, distance = " + currentDistance + " cm");
        } while(currentDistance > 10);

        color = readColor();
        while (color != Color.NONE && color != Color.BLACK) {
            System.out.println("Found color: " + color);
            Thread.sleep(2000);
            switch (color) {
                case BLUE:
                    System.out.println("Case: BLUE");
                    dropBrick(21);
                    break;
                case GREEN:
                    System.out.println("Case: GREEN");
                    dropBrick(33);
                    break;
                case YELLOW:
                    System.out.println("Case: YELLOW");
                    dropBrick(45);
                    break;
                case WHITE:
                    System.out.println("Case: WHITE");
                    dropBrick(0);
                    break;
                case RED:
                    System.out.println("Case: RED");
                    dropBrick(10);
                    break;
                default:
                    System.out.println("ERROR: Default case!");
            }
            color = readColor();
        }

        System.out.println("Color detected: " + color + ", FINISHED!");
        brikkeMotor.rotateTo(70, false);
    }

    /**
     * Reverses a set distance before dropping a colored brick (by rotating the brikkeMotor),
     * then drives forward until 10 cm away from a static object/beacon.
     * @param targetDistance The distance to reverse to before dropping the brick
     * @throws InterruptedException Occurs if the thread (for sleeping) gets interrupted.
     */
    private static void dropBrick(int targetDistance) throws InterruptedException {
        int currentDistance;

        // Reverses the large motors
        motorB.forward();
        motorC.backward();

        do {
            currentDistance = getDistance();
            System.out.println("Reversing for drop-off, distance =  " + currentDistance + " cm");
        } while(currentDistance < targetDistance);

        motorB.stop();
        motorC.stop();
        System.out.println("Drop-off point reached, dropping payload!");
        brikkeMotor.rotate(360);
        Thread.sleep(1000);

        // Moves the large motors forward
        motorB.backward();
        motorC.forward();

        do {
            currentDistance = getDistance();
            System.out.println("Driving forward to beacon-point, distance = " + currentDistance + " cm");
        } while(currentDistance > 10);

        motorB.stop();
        motorC.stop();
        System.out.println("Beacon-point reached, beginning new drop-off process!");
    }

    /**
     * Gets the distance reported from the ultrasonic sensor to the nearest object
     * @return int, with the distance in cm
     */
    private static int getDistance() {
        float[] sample = new float[sp.sampleSize()];
        sp.fetchSample(sample, 0);
        return (int)sample[0];
    }

    /**
     * Gets the current color as a Color enum from the color-sensor
     * @return Color enum
     */
    private static Color readColor() {
        int colorNum = colorSensor.getColorID();
        switch (colorNum) {
            case 1:
                return Color.BLACK;
            case 2:
                return Color.BLUE;
            case 3:
                return Color.GREEN;
            case 4:
                return Color.YELLOW;
            case 5:
                return Color.RED;
            case 6:
                return Color.WHITE;
            case 7:
                return Color.BROWN;
            default:
                return Color.NONE;
        }
    }
}